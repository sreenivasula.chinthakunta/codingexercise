package com.codingexercise.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.codingexercise.model.Topic;
import com.codingexercise.service.TopicService;


 
@ExtendWith(MockitoExtension.class)
public class TopicControllerTest 
{
    @InjectMocks
    TopicController topicController;
     
    @Mock
    TopicService service;
    
        
    @Test
    public void testSaveTopic() throws Exception 
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
         
        when(service.saveTopic(any(Topic.class))).thenReturn(new Topic());
         
        Topic topic = new Topic("A", "a", "testingtestingtesting");
        ResponseEntity<Object> responseEntity = topicController.saveTopics(topic);
         
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);       
    }
    
         
    @Test
    public void testFindAll() 
    {
        // given
    	Topic topicA = new Topic("A", "a", "testingtestingtestingtesting");
    	Topic topicB = new Topic("B", "x", "testingtestingtesting");
        List<Topic> topics = new ArrayList<>();
        topics.addAll(Arrays.asList(topicA, topicB));
 
        when(service.getAllTopics()).thenReturn(topics);
 
        // when
        List<Topic> results = topicController.getAllTopics();
 
        // then
        assertThat(results.size()).isEqualTo(2);
         
        assertThat(results.get(0).getTopic())
                        .isEqualTo(topicA.getTopic());
         
        assertThat(results.get(1).getTopic())
        .isEqualTo(topicB.getTopic());
    }
}
