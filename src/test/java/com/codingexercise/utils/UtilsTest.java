package com.codingexercise.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import com.codingexercise.model.Topic;
import com.codingexercise.util.Utils;

public class UtilsTest {
	
	
	@Test
    public void testValidateTopicNameSuccess() throws Exception 
    {
         
        Topic topic = new Topic("A", "a", "testingtestingtesting");
        String message = Utils.validateValue(topic);  
        assertEquals(null, message);
               
    }
	
	@Test
    public void testValidateTopicName_Error() throws Exception 
    {
         
        Topic topic = new Topic("A", "aa", "testingtestingtesting");
        String message = Utils.validateValue(topic);  
        assertEquals("Name must be 'a'", message);
               
    }
	
	@Test
    public void testValidateDescriptionLength_Error() throws Exception 
    {
         
        Topic topic = new Topic("A", "a", "testingte");
        String message = Utils.validateLength(topic);  
        assertEquals("Description length must be in btween 10 & 100", message);
               
    }
	
	@Test
    public void testValidateDescriptionLength1_Error() throws Exception 
    {
         
        Topic topic = new Topic("A", "a", "testingte");
        String message = Utils.validateLength(topic);  
        assertNotNull(message);
               
    }
	
	
	    

}
