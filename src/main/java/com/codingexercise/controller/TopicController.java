package com.codingexercise.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codingexercise.model.Topic;
import com.codingexercise.service.TopicService;
import com.codingexercise.util.TopicResponse;
import com.codingexercise.util.Utils;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService service;
	
	@GetMapping(path = "/topic", produces = "application/json")
	@ResponseBody
	public Topic getTopics(@RequestParam(name="topic") long id) {
		return service.getTopic(id);
	}
	
	@GetMapping(path = "/topics", produces = "application/json")
	@ResponseBody
	public List<Topic> getAllTopics() {
		return service.getAllTopics();
	}
	
	@PostMapping(path = "/topic", consumes = "application/json")
	public ResponseEntity<Object> saveTopics(@RequestBody Topic topic) throws Exception{
		List<String> errors = new ArrayList<String>();
		
		System.out.println("topic: "+topic);
		
		//Validate name for given topic
		if(Utils.validateValue(topic) != null) {
			errors.add(Utils.validateValue(topic));
		}
		
		//Validate description length for given topic
		if(Utils.validateLength(topic) != null) {
			errors.add(Utils.validateLength(topic));
		}
		
		//When validation errors are occured.
		if(errors.size() > 0) {
			return new ResponseEntity<Object>(new TopicResponse("400", "ValidationError", errors.stream().map(Object::toString).collect(Collectors.joining(","))), HttpStatus.BAD_REQUEST);				    
		}		
		
		return new ResponseEntity<Object>(service.saveTopic(topic), HttpStatus.OK);
	}

}
