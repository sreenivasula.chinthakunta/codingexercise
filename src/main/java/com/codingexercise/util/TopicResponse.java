package com.codingexercise.util;

import java.util.Arrays;
import java.util.List;

public class TopicResponse {

    private String errorCode;
    private String errorType;
    private List<String> errorMessage;

    public TopicResponse(String errorCode, String errorType, List<String> errorMessage) {
        super();
        this.errorCode = errorCode;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    public TopicResponse(String status, String message, String error) {
        super();
        this.errorCode = status;
        this.errorType = message;
        this.errorMessage = Arrays.asList(error);
    }

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public List<String> getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(List<String> errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
