package com.codingexercise.util;

import java.util.Arrays;

import com.codingexercise.enums.TopicName;
import com.codingexercise.model.Topic;

public class Utils {
	
	 	
	public static String validateValue(Topic topic) {
		
		String message = null;
		if(topic.getTopic() != null)
			switch (topic.getTopic()) {
			 case "A": if(!"a".equals(topic.getName())) message = "Name must be 'a'"; break;
			 case "B": if(!"x".equals(topic.getName())) message = "Name must be 'x'";break ;
			 case "C": if(!"c".equals(topic.getName())) message = "Name must be 'c'";break ;
			 default: message = "Allowed topics are "+Arrays.asList(TopicName.values());
		}
		
		return message;
	}
	
	public static String validateLength(Topic topic) {
		
		String message = null;
		var length = 0;
		if(topic.getTopic() != null && topic.getDescription() != null)
			length = topic.getDescription().length();
			switch (topic.getTopic()) {
			 case "A": if(length < 10 || length > 100 ) message = "Description length must be in btween 10 & 100"; break ;
			 case "B": if(length > 40) message = "Description length must be lessthan 40"; break ;			
		}
		
		return message;		
		
	}

}
