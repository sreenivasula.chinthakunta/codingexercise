package com.codingexercise.service;

import java.util.List;

import com.codingexercise.model.Topic;


public interface TopicService {
	
	public Topic saveTopic(Topic topic);
	public Topic getTopic(long id);
	public List<Topic> getAllTopics();

}
