package com.codingexercise.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codingexercise.model.Topic;
import com.codingexercise.repository.TopicRepository;

@Service
public class TopicServiceImpl implements TopicService {
	
	@Autowired
	TopicRepository repository;
	
	@Transactional
	public Topic saveTopic(Topic topic) {
		Topic topicNew =  repository.save(topic);		
		return topicNew;
	}
	
	@Transactional
	public Topic getTopic(long id) {
		return repository.findById(id);
	}

	@Override
	public List<Topic> getAllTopics() {
		// TODO Auto-generated method stub
		return (List<Topic>) repository.findAll();
	}
	
	

}
