package com.codingexercise.repository;

import org.springframework.data.repository.CrudRepository;

import com.codingexercise.model.Topic;

public interface TopicRepository extends CrudRepository<Topic, Long> {
	
	Topic findById(long id);

}
